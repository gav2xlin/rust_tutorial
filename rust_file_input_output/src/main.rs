use std::io::Write;
use std::io::Read;
use std::fs;
use std::fs::OpenOptions;

fn main() {
    {
        let mut file = std::fs::File::create("data.txt").expect("create failed");
        file.write_all("Hello World".as_bytes()).expect("write failed");
        file.write_all("\nTutorialsPoint".as_bytes()).expect("write failed");
        println!("data written to file" );
    }

    {
        let mut file = std::fs::File::open("data.txt").unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        print!("{}", contents);
    }

    {
        fs::remove_file("data.txt").expect("could not remove file");
        println!("file is removed");
    }

    {
        let mut file = OpenOptions::new().append(true).open("data.txt").expect("cannot open file");
        file.write_all("Hello World".as_bytes()).expect("write failed");
        file.write_all("\nTutorialsPoint".as_bytes()).expect("write failed");
        println!("file append success");
    }

    {
        let mut command_line: std::env::Args = std::env::args();
        command_line.next().unwrap();
        // skip the executable file name
        // accept the source file
        let source = command_line.next().unwrap();
        // accept the destination file
        let destination = command_line.next().unwrap();
        let mut file_in = std::fs::File::open(source).unwrap();
        let mut file_out = std::fs::File::create(destination).unwrap();
        let mut buffer = [0u8; 4096];
        loop {
            let nbytes = file_in.read(&mut buffer).unwrap();
            file_out.write(&buffer[..nbytes]).unwrap();
            if nbytes < buffer.len() { break; }
        }
    }
}
