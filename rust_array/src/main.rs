fn main() {
    {
        let arr:[i32;4] = [10,20,30,40];
        println!("array is {:?}",arr);
        println!("array size is :{}",arr.len());
    }

    {
        let arr = [10,20,30,40];
        println!("array is {:?}",arr);
        println!("array size is :{}",arr.len());
    }

    {
        let arr:[i32;4] = [-1;4];
        println!("array is {:?}",arr);
        println!("array size is :{}",arr.len());
    }

    {
        let arr:[i32;4] = [10,20,30,40];
        println!("array is {:?}",arr);
        println!("array size is :{}",arr.len());

        for index in 0..4 {
            println!("index is: {} & value is : {}",index,arr[index]);
        }
    }

    {
        let arr:[i32;4] = [10,20,30,40];
        println!("array is {:?}",arr);
        println!("array size is :{}",arr.len());

        for val in arr.iter(){
            println!("value is :{}",val);
        }
    }

    {
        let mut arr:[i32;4] = [10,20,30,40];
        arr[1] = 0;
        println!("{:?}",arr);
    }

    {
        let arr = [10,20,30];
        update(arr);

        print!("Inside main {:?}",arr);
    }

    {
        let mut arr = [10,20,30];
        _update(&mut arr);
        print!("Inside main {:?}",arr);
    }

    {
        /*let N: usize = 20;
        let arr = [0; N]; //Error: non-constant used with constant
        print!("{}",arr[10])*/
    }

    {
        const N: usize = 20;
        // pointer sized
        let arr = [0; N];

        print!("{}",arr[10])
    }
}

fn update(mut arr:[i32;3]){
    for i in 0..3 {
        arr[i] = 0;
    }
    println!("Inside update {:?}",arr);
}

fn _update(arr:&mut [i32;3]){
    for i in 0..3 {
        arr[i] = 0;
    }
    println!("Inside update {:?}",arr);
}