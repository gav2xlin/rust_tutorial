fn main() {
    {
        let num1 = 10;
        let num2 = 2;
        let mut res: i32;

        res = num1 + num2;
        println!("Sum: {} ", res);

        res = num1 - num2;
        println!("Difference: {} ", res);

        res = num1 * num2;
        println!("Product: {} ", res);

        res = num1 / num2;
        println!("Quotient: {} ", res);

        res = num1 % num2;
        println!("Remainder: {} ", res);
    }

    {
        let A:i32 = 10;
        let B:i32 = 20;

        println!("Value of A:{} ",A);
        println!("Value of B : {} ",B);

        let mut res = A>B ;
        println!("A greater than B: {} ",res);

        res = A<B ;
        println!("A lesser than B: {} ",res) ;

        res = A>=B ;
        println!("A greater than or equal to B: {} ",res);

        res = A<=B;
        println!("A lesser than or equal to B: {}",res) ;

        res = A==B ;
        println!("A is equal to B: {}",res) ;

        res = A!=B ;
        println!("A is not equal to B: {} ",res);
    }

    {
        let a = 20;
        let b = 30;

        if (a > 10) && (b > 10) {
            println!("true");
        }
        let c = 0;
        let d = 30;

        if (c>10) || (d>10){
            println!("true");
        }
        let is_elder = false;

        if !is_elder {
            println!("Not Elder");
        }
    }

    {
        let a:i32 = 2;     // Bit presentation 10
        let b:i32 = 3;     // Bit presentation 11

        let mut result:i32;

        result = a & b;
        println!("(a & b) => {} ",result);

        result = a | b;
        println!("(a | b) => {} ",result) ;

        result = a ^ b;
        println!("(a ^ b) => {} ",result);

        result = !b;
        println!("(!b) => {} ",result);

        result = a << b;
        println!("(a << b) => {}",result);

        result = a >> b;
        println!("(a >> b) => {}",result);
    }
}