struct Employee {
    name:String,
    company:String,
    age:u32
}

fn main() {
    {
        let emp1 = Employee {
            company: String::from("TutorialsPoint"),
            name: String::from("Mohtashim"),
            age: 50
        };
        println!("Name is :{} company is {} age is {}", emp1.name, emp1.company, emp1.age);
    }

    {
        let mut emp1 = Employee {
            company:String::from("TutorialsPoint"),
            name:String::from("Mohtashim"),
            age:50
        };
        emp1.age = 40;
        println!("Name is :{} company is {} age is {}",emp1.name,emp1.company,emp1.age);
    }

    {
        //initialize a structure
        let emp1 = Employee {
            company:String::from("TutorialsPoint"),
            name:String::from("Mohtashim"),
            age:50
        };
        let emp2 = Employee{
            company:String::from("TutorialsPoint"),
            name:String::from("Kannan"),
            age:32
        };
        //pass emp1 and emp2 to display()
        display(emp1);
        display(emp2);
    }

    {
        //initialize structure
        let emp1 = Employee{
            company:String::from("TutorialsPoint"),
            name:String::from("Mohtashim"),
            age:50
        };
        let emp2 = Employee {
            company:String::from("TutorialsPoint"),
            name:String::from("Kannan"),
            age:32
        };
        let elder = who_is_elder(emp1,emp2);
        println!("elder is:");

        //prints details of the elder employee
        display(elder);
    }

    {
        // instantiate the structure
        let small = Rectangle {
            width:10,
            height:20
        };
        //print the rectangle's area
        println!("width is {} height is {} area of Rectangle is {}",small.width,small.height,small.area());
    }

    {
        // Invoke the static method
        let p1 = Point::get_instance(10, 20);
        p1.display();
    }
}

// fetch values of specific structure fields using the
// operator and print it to the console
fn display( emp:Employee){
    println!("Name is :{} company is {} age is {}",emp.name,emp.company,emp.age);
}

//accepts instances of employee structure and compares their age
fn who_is_elder (emp1:Employee,emp2:Employee)->Employee {
    /*if emp1.age>emp2.age {
        return emp1;
    } else {
        return emp2;
    }*/
    return if emp1.age > emp2.age {
        emp1
    } else {
        emp2
    }
}

//define dimensions of a rectangle
struct Rectangle {
    width:u32, height:u32
}

//logic to calculate area of a rectangle
impl Rectangle {
    fn area(&self)->u32 {
        //use the . operator to fetch the value of a field via the self keyword
        self.width * self.height
    }
}

//declare a structure
struct Point {
    x: i32,
    y: i32,
}
impl Point {
    //static method that creates objects of the Point structure
    fn get_instance(x: i32, y: i32) -> Point {
        Point { x, y }
    }
    //display values of the structure's field
    fn display(&self){
        println!("x ={} y={}",self.x,self.y );
    }
}