use std::io::Write;

fn main() {
    {
        let mut line = String::new();
        println!("Enter your name :");
        let b1 = std::io::stdin().read_line(&mut line).unwrap();
        println!("Hello , {}", line);
        println!("no of bytes read , {}", b1);
    }

    {
        let b1 = std::io::stdout().write("Tutorials ".as_bytes()).unwrap();
        let b2 = std::io::stdout().write(String::from("Point").as_bytes()).unwrap();
        std::io::stdout().write(format!("\nbytes written {}",(b1+b2)).as_bytes()).unwrap();
    }

    {
        let cmd_line = std::env::args();
        println!("No of elements in arguments is :{}",cmd_line.len());
        //print total number of values passed
        for arg in cmd_line {
            println!("[{}]",arg); //print all values passed as commandline arguments
        }
    }

    {let cmd_line = std::env::args();
        println!("No of elements in arguments is
   :{}",cmd_line.len());
        // total number of elements passed

        let mut sum = 0;
        let mut has_read_first_arg = false;

        //iterate through all the arguments and calculate their sum

        for arg in cmd_line {
            if has_read_first_arg { //skip the first argument since it is the exe file name
                sum += arg.parse::<i32>().unwrap();
            }
            has_read_first_arg = true;
            // set the flag to true to calculate sum for the subsequent arguments.
        }
        println!("sum is {}",sum);}
}
