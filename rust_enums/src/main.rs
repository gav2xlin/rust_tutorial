// The `derive` attribute automatically creates the implementation
// required to make this `enum` printable with `fmt::Debug`.
#[derive(Debug)]
enum GenderCategory {
    Male,Female
}

// The `derive` attribute automatically creates the implementation
// required to make this `enum` printable with `fmt::Debug`.
#[derive(Debug)]
enum _GenderCategory {
    Name(String),
    UsrId(i32)
}

// The `derive` attribute automatically creates the implementation
// required to make this `struct` printable with `fmt::Debug`.
#[derive(Debug)]
struct Person {
    name:String,
    gender:GenderCategory
}

fn main() {
    {
        let male = GenderCategory::Male;
        let female = GenderCategory::Female;

        println!("{:?}", male);
        println!("{:?}", female);
    }

    {
        let p1 = Person {
            name:String::from("Mohtashim"),
            gender:GenderCategory::Male
        };
        let p2 = Person {
            name:String::from("Amy"),
            gender:GenderCategory::Female
        };
        println!("{:?}",p1);
        println!("{:?}",p2);
    }

    {
        let result = is_even(3);
        println!("{:?}",result);
        println!("{:?}",is_even(30));
    }

    {
        print_size(CarType::SUV);
        print_size(CarType::Hatch);
        print_size(CarType::Sedan);
    }

    {
        match is_even(5) {
            Some(data) => {
                if data==true {
                    println!("Even no");
                }
            },
            None => {
                println!("not even");
            }
        }
    }

    {
        let p1 = _GenderCategory::Name(String::from("Mohtashim"));
        let p2 = _GenderCategory::UsrId(100);
        println!("{:?}",p1);
        println!("{:?}",p2);

        match p1 {
            _GenderCategory::Name(val)=> {
                println!("{}",val);
            }
            _GenderCategory::UsrId(val)=> {
                println!("{}",val);
            }
        }
    }
}

enum CarType {
    Hatch,
    Sedan,
    SUV
}

fn print_size(car:CarType) {
    match car {
        CarType::Hatch => {
            println!("Small sized car");
        },
        CarType::Sedan => {
            println!("medium sized car");
        },
        CarType::SUV =>{
            println!("Large sized Sports Utility car");
        }
    }
}

fn is_even(no:i32)->Option<bool> {
    if no%2 == 0 {
        Some(true)
    } else {
        None
    }
}
