fn main() {
    {
        let var_i32 = 5;
        //stack
        let b = Box::new(var_i32);
        //heap
        println!("b = {}", b);
    }

    {
        let x = 5;
        //value type variable
        let y = Box::new(x);
        //y points to a new value 5 in the heap

        println!("{}",5==x);
        println!("{}",5==*y);
        //dereferencing y
    }

    {
        let x = 5;
        let y = MyBox::new(x);
        // calling static method

        println!("5==x is {}",5==x);
        println!("5==*y is {}",5==*y);
        // dereferencing y
        println!("x==*y is {}",x==*y);
        //dereferencing y
    }

    {
        let x = 50;
        MyBox::new(x);
        MyBox::new("Hello");
    }
}

use std::ops::Deref;

struct MyBox<T>(T);

impl<T> MyBox<T> {
    fn new(x:T)->MyBox<T>{
        MyBox(x)
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T;
    fn deref(&self) ->&T {
    &self.0
    }
}

impl<T> Drop for MyBox<T>{
    fn drop(&mut self){
        println!("dropping MyBox object from memory ");
    }
}