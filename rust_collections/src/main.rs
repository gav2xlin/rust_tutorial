use std::collections::HashMap;
use std::collections::HashSet;

fn main() {
    {
        let mut v = Vec::new();
        v.push(20);
        v.push(30);
        v.push(40);

        println!("size of vector is :{}", v.len());
        println!("{:?}", v);
    }

    {
        let v = vec![1,2,3];
        println!("{:?}",v);
    }

    {
        /*let v = vec![1,2,3,"hello"];
        println!("{:?}",v);*/
    }

    {
        let mut v = Vec::new();
        v.push(20);
        v.push(30);
        v.push(40);

        println!("{:?}",v);
    }

    {
        let mut v = vec![10,20,30];
        v.remove(1);
        println!("{:?}",v);
    }

    {
        let v = vec![10,20,30];
        if v.contains(&10) {
            println!("found 10");
        }
        println!("{:?}",v);
    }

    {
        let v = vec![1,2,3];
        println!("size of vector is :{}",v.len());
    }

    {
        let mut v = Vec::new();
        v.push(20);
        v.push(30);

        println!("{:?}",v[0]);
    }

    {
        let mut v = Vec::new();
        v.push(20);
        v.push(30);
        v.push(40);
        v.push(500);

        for i in &v {
            println!("{}",i);
        }
        println!("{:?}",v);
    }

    {
        let mut stateCodes = HashMap::new();
        stateCodes.insert("KL","Kerala");
        stateCodes.insert("MH","Maharashtra");
        println!("{:?}",stateCodes);
    }

    {
        let mut stateCodes = HashMap::new();
        stateCodes.insert("KL","Kerala");
        stateCodes.insert("MH","Maharashtra");
        println!("size of map is {}",stateCodes.len());
    }

    {
        let mut stateCodes = HashMap::new();
        stateCodes.insert("KL","Kerala");
        stateCodes.insert("MH","Maharashtra");
        println!("size of map is {}",stateCodes.len());
        println!("{:?}",stateCodes);

        match stateCodes.get(&"KL") {
            Some(value)=> {
                println!("Value for key KL is {}",value);
            }
            None => {
                println!("nothing found");
            }
        }
    }

    {
        let mut stateCodes = HashMap::new();
        stateCodes.insert("KL","Kerala");
        stateCodes.insert("MH","Maharashtra");

        for (key, val) in stateCodes.iter() {
            println!("key: {} val: {}", key, val);
        }
    }

    {
        let mut stateCodes = HashMap::new();
        stateCodes.insert("KL","Kerala");
        stateCodes.insert("MH","Maharashtra");
        stateCodes.insert("GJ","Gujarat");

        if stateCodes.contains_key(&"GJ") {
            println!("found key");
        }
    }

    {
        let mut stateCodes = HashMap::new();
        stateCodes.insert("KL","Kerala");
        stateCodes.insert("MH","Maharashtra");
        stateCodes.insert("GJ","Gujarat");

        println!("length of the hashmap {}",stateCodes.len());
        stateCodes.remove(&"GJ");
        println!("length of the hashmap after remove() {}",stateCodes.len());
    }

    {
        let mut names = HashSet::new();

        names.insert("Mohtashim");
        names.insert("Kannan");
        names.insert("TutorialsPoint");
        names.insert("Mohtashim");//duplicates not added

        println!("{:?}",names);
    }

    {
        let mut names = HashSet::new();
        names.insert("Mohtashim");
        names.insert("Kannan");
        names.insert("TutorialsPoint");
        println!("size of the set is {}",names.len());
    }

    {
        let mut names = HashSet::new();
        names.insert("Mohtashim");
        names.insert("Kannan");
        names.insert("TutorialsPoint");
        names.insert("Mohtashim");

        for name in names.iter() {
            println!("{}",name);
        }
    }

    {
        let mut names = HashSet::new();
        names.insert("Mohtashim");
        names.insert("Kannan");
        names.insert("TutorialsPoint");
        names.insert("Mohtashim");

        match names.get(&"Mohtashim"){
            Some(value)=>{
                println!("found {}",value);
            }
            None =>{
                println!("not found");
            }
        }
        println!("{:?}",names);
    }

    {
        let mut names = HashSet::new();
        names.insert("Mohtashim");
        names.insert("Kannan");
        names.insert("TutorialsPoint");

        if names.contains(&"Kannan") {
            println!("found name");
        }
    }

    {
        let mut names = HashSet::new();
        names.insert("Mohtashim");
        names.insert("Kannan");
        names.insert("TutorialsPoint");
        println!("length of the Hashset: {}",names.len());
        names.remove(&"Kannan");
        println!("length of the Hashset after remove() : {}",names.len());
    }
}